<?php

/**
 * Implements hook_menu().
 */
function plinko_menu() {
  $items['admin/appearance/plinko'] = array(
    'title' => 'Plinko',
    'page callback' => 'plinko_settings_page',
    'access arguments' => array('administer themes'),
    'type' => MENU_SUGGESTED_ITEM,
  );

  return $items;
}

function plinko_settings_page() {
  return '<div class="' . drupal_html_class('plinko_settings_form') . '">' . drupal_render(drupal_get_form('plinko_settings_form')) . '</div>'
          . '<div class="' . drupal_html_class('plinko_add_setting_form') . '">'. drupal_render(drupal_get_form('plinko_add_setting_form')) . '</div>';
}

function plinko_add_setting_form($form, &$form_state) {
  $form['lookup'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#description' => t('Path field description text goes here.'),
    '#size' => 40,
    '#maxlength' => 255,
  );
  
  
  $form['create_new_setting'] = array(
    '#type' => 'submit',
    '#value' => t('Create new setting'),
  );
  return $form;
}

/**
 * 
 */
function plinko_settings_form($form, &$form_state) {
  $form = array();
  $settings = _plink_list_settings();
  drupal_add_css(drupal_get_path('module', 'plinko') . '/css/styles.css');
  
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#access' => user_access('administer themes'),
  );

  foreach ($settings as $plid => $setting) {
    $settings[$plid]['settings'] = unserialize($setting['settings']);
    
    // Create the array if the 'settings' index started out uninitialized
    if (!is_array($settings[$plid]['settings'])) {
      $settings[$plid]['settings'] = array();
    }
    
    // Iterate through each setting and create a hidden field to hold the values to them
    foreach ($settings[$plid]['settings'] as $key => $val) {
      $form['settings'][$key] = array(
        '#type' => 'hidden',
        '#value' => $val,
      );
    }
  }
  
  $form['settings_holder'] = array(
    '#markup' => theme('plinko_settings', array('settings' => $settings)),
    '#prefix' => '<div class="tabs settings-holder">',
    '#suffix' => '</div>',
  );
  
  _plink_attach_visible_form_widgets($form);
  
  $form['save_settings'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );
  
  return $form;
}

function _plink_list_settings() {
  $result = db_select('plinko', 'p')
              ->fields('p')
              ->execute()
              ->fetchAllAssoc('plid');
  foreach ($result as $key => $val) {
    $result[$key] = (array) $val;
  }
  return $result;
}

/**
 * Implements hook_theme().
 */
function plinko_theme($existing, $type, $theme, $path) {
  return array(
    'plinko_settings' => array(
      'variables' => array('settings' => array()),
    ),
    'plinko_settings_tabs' => array(
      'variables' => array('settings' => array()),
    ),
  );
}

function theme_plinko_settings($vars = array()) {
  $output = theme('plinko_settings_tabs', $vars);
  return $output;
}

function theme_plinko_settings_tabs($vars = array()) {
  if (isset($vars['settings']) && is_array($vars['settings']) && count($vars['settings'])) {
    $output = '';
    foreach ($vars['settings'] as $setting) {
      $output .= '<li class="">' . l($setting['lookup'], 'admin/appearance/plinko/' . $setting['plid']) . '</li>';
    }
    return '<ul class="" id="plinko-settings-tabs">' . $output . '</ul>';
  }
  return '';
}

function _plink_attach_visible_form_widgets(&$form) {
  require_once(drupal_get_path('theme', 'plink') . '/theme-settings.php');
  
  $form['visible_form_widgets_opening'] = array(
    '#markup' => '<div class="visible-form-widgets">',
  );
  $form['grid_system'] = array(
    '#type' => 'fieldset',
    '#title' => t('960 Grid System Settings'),
    '#description' => t('Enable / Disable and configure the grid system settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#access' => user_access('Administer themes'),
  );

  $form['grid_system']['enable_grid_system'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable 960 Grid System'),
    //'#default_value' => plink_theme_get_setting('enable_grid_system',$theme_key),
  );
  
  $form['grid_system']['choose_a_grid_system'] = array(
    '#type' => 'select',
    '#title' => t('Choose a grid system'),
    '#description' => t(''),
    '#options' => array(
      '0' => t('12 Column - Fixed'),
      '1' => t('16 Column - Fixed'),
      '2' => t('24 Column - Fixed'),
      '3' => t('12 Column - Fluid'),
      '4' => t('16 Column - Fluid'),
    ),
    //'#default_value' => plink_theme_get_setting('choose_a_grid_system',$theme_key),
    '#states' => array(
      'visible' => array(
        ':input[name="enable_grid_system"]' => array('checked' => TRUE),
      ),
    ),
  );
  
  $form['grid_system']['page_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Width'),
    '#description' => t('If you are not using a grid based system please enter your page width here.'),
    '#size' => 40,
    '#maxlength' => 8,
    //'#default_value' => plink_theme_get_setting('page_width',$theme_key),
    '#field_suffix' => t(' px'),
    '#states' => array(
      'visible' => array(
        ':input[name="enable_grid_system"]' => array('checked' => FALSE),
      ),
    ),
  );
  
 // ----------------------------------------------------------------------------------------
 // Screen Content Layouts
 // ----------------------------------------------------------------------------------------

  $form['content_layouts'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Layouts'),
    '#description' => t('Below are the settings for your main content regions. Please choose a content layout from the right and set the widths of the sidebars below. The Primary [1] region will always take up the remaining space. The secondary [2] and tertiary [3] regions are fully collapsible. This means if they do not have any blocks or content in them they will not show on your page and the primary region [1] will fill in the space that the collapsed region would have used.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#access' => user_access('Administer themes'),
  );

  //
  // LEFT COLUMN
  //

  $form['content_layouts']['left_column_wrapper'] = array(
    '#markup' => '<div class="settings-left-column">',
  );

  $form['content_layouts']['main_secondary_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Secondary [2] Width'),
    '#size' => 20,
    '#maxlength' => 8,
    '#prefix' => '<strong class="setting-heading">' . t('Sidebar Width Settings') . '</strong>',
    '#attributes' => array('class'=>array('manualsizeinput')),
    '#field_suffix' => t(' px'),
    //'#default_value' => plink_theme_get_setting('main_secondary_width', $theme_key),
  );

  $form['content_layouts']['main_tertiary_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Tertiary [3] Width'),
    '#size' => 20,
    '#maxlength' => 8,
    '#attributes' => array('class'=>array('manualsizeinput')),
    '#field_suffix' => t(' px'),
    //'#default_value' => plink_theme_get_setting('main_tertiary_width', $theme_key),
  );  

  $form['content_layouts']['left_column_wrapper_closure'] = array(
    '#markup' => '</div>',
  );

  //
  // RIGHT COLUMN
  //

  $form['content_layouts']['right_column_wrapper'] = array(
    '#markup' => '<div class="settings-right-column">',
  );

  $form['content_layouts']['heading'] = array(
    '#markup' => t('Content Layout Options'),
    '#prefix' => '<strong class="setting-heading">',
    '#suffix' => '</strong>',
  );

  $form['content_layouts']['main_layout'] = array(
    '#type' => 'radios',
    '#options' => plink_theme_get_layout_options('main'),
    //'#default_value' => plink_theme_get_setting('main_layout',$theme_key),
  );



  $form['content_layouts']['right_column_wrapper_closure'] = array(
    '#markup' => '</div>',
  );


// ----------------------------------------------------------------------------------------
// Paye Layout Options
// ---------------------------------------------------------------------------------------- 

  $form['page_layout_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page layout options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#access' => user_access('Administer themes'),
  );

  //
  // LEFT COLUMN
  //

  $form['page_layout_options']['left_column_wrapper'] = array(
    '#markup' => '<div class="settings-left-column">',
  );

  $form['page_layout_options']['full_width_regions'] = array(
    '#type' => 'checkboxes',
    '#title' => t(''),
    '#prefix' => '<strong class="setting-heading">' . t('100% width regions') . '</strong>',
    '#options' => array(
      'header' => 'header',
      'preface_first' => 'preface_first',
      'preface' => 'preface',
      'preface_last' => 'preface_last',
      'postscript_first' => 'postscript_first',
      'postscript' => 'postscript',
      'postscript_last' => 'postscript_last',
      'footer' => 'footer',
      ),
    //'#default_value' => plink_theme_get_setting('full_width_regions', $theme_key),  
  );

  $form['page_layout_options']['left_column_wrapper_closure'] = array(
    '#markup' => '</div>',
  );

  //
  // RIGHT COLUMN
  //

  $page_layout_preview = file_get_contents(DRUPAL_ROOT . '/' . drupal_get_path('theme', 'plink') . '/engine/page_layout_preview.html');

  $form['page_layout_options']['right_column_wrapper'] = array(
    '#markup' => '<div class="settings-right-column">',
  );

  $form['page_layout_options']['heading'] = array(
    '#markup' => t('Layout Preview'),
    '#prefix' => '<strong class="setting-heading">',
    '#suffix' => '</strong>',
  );

  $form['page_layout_options']['page_layout_preview'] = array(
    '#markup' => $page_layout_preview,
  );



  $form['page_layout_options']['right_column_wrapper_closure'] = array(
    '#markup' => '</div>',
  );
  
  $form['visible_form_widgets_closing'] = array(
    '#markup' => '</div>',
  );
}